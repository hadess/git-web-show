# git-web-show

Show the current directory or the passed file path in a web interface,
so as to make sharing easier.

```sh
$ git web-show lib/gws-lib.lua
```
will open a browser window that shows the file in question.

## Download

Script lives in [bin/](bin/). Install it in your path.

## Development

If you're interested in development, you'll need to have a checkout of
[lua-amalg](https://github.com/siffiejoe/lua-amalg) next to your
`git-web-show` checkout to generate the updated script. Simply run `make`
to create the updated script, `make install` to install it in `~/.local/bin`,
and `make check` to run the tests in `run-tests.lua`.

## License

See [COPYING](COPYING).
