--[[
 * Copyright (C) 2016,2019 Red Hat, Inc.
 * Author: Bastien Nocera <hadess@hadess.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA.
 *
--]]

local gws = require "lib.gws-lib"
local posix = require "posix"

-- Test the URL parser
local remote_url = get_remote_url()
assert(remote_url)

local domain0, path0 = get_domain("https://github.com/pulkomandy/thomson.git")
assert(domain0 == 'github.com')
assert(path0 == '/pulkomandy/thomson.git')

local domain1, path1 = get_domain("git@gitlab.gnome.org:GNOME/glib.git")
assert(domain1 == 'gitlab.gnome.org')
assert(path1 == '/GNOME/glib.git')

local url0 = get_url('gitlab.freedesktop.org', '/hadess/git-web-show.git', nil, 'README.md')
assert(url0 == 'https://gitlab.freedesktop.org/hadess/git-web-show/blob/master/README.md')
