bin/git-web-show: git-web-show.lua lib/gws-lib.lua
	mkdir -p bin
	../lua-amalg/src/amalg.lua -o bin/git-web-show -s git-web-show.lua lib.gws-lib
	chmod a+x bin/git-web-show

check: run-tests.lua
	lua run-tests.lua

install: bin/git-web-show
	mkdir -p ~/.local/bin/
	install -m755 bin/git-web-show ~/.local/bin/
