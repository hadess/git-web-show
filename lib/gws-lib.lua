-- TODO
-- - Add support for gist.github.com
-- - Fix handling of directories

function usage()
	print ("git web-show [--print] [FILE...]")
end

-- From http://w3.impa.br/~diego/software/luasocket/url.html
U = require("socket.url")

-- From http://stackoverflow.com/questions/132397/get-back-the-output-of-os-execute-in-lua
--
function get_stdout(command)
	local f = assert(io.popen(command, 'r'))
	local s = assert(f:read('*a'))
	f:close()
	return s
end

function get_remote_url()
	local remote_url = os.getenv ('GIT_WEB_SHOW_REMOTE_ORIGIN_URL') or
	                   get_stdout("git config --get remote.origin.url")
	remote_url = remote_url:gsub("\n", '')
	return remote_url
end

function get_file(file)
	if not file then return nil end
	-- FIXME won't work if file is a directory
	local cmd = "git ls-files --full-name " .. file
	local fullpath = get_stdout(cmd)
	if fullpath == '' then
		return nil
	end
	return fullpath:gsub("\n", '')
end

function get_domain(url)
	-- Fixup git@ URLs
	if not url:match('%://') then
		-- From git@github.com:foo/bar.git
		-- to https://git@github.com/foo/bar.git
		url = url:gsub('%:','/')
		url = 'https://' .. url
	end
	parsed_url = U.parse(url)
	local host = parsed_url.host .. (parsed_url.port or "")
	return host, parsed_url.path
end

function remove_first_element(path)
	return path:gsub("^/git", '')
end

function get_url(domain, path, branch, file)
	-- print ('domain: ' .. (domain or 'nil'))
	-- print ('path: ' .. (path or 'nil'))
	-- print ('branch: ' .. (branch or 'nil'))
	-- print ('file: ' .. (file or 'nil'))
	local no_git_path = remove_first_element(path)
	if file == nil then file = "" end
	if branch == nil then branch_frag = "" else branch_frag = ("?h=" .. branch) end
	if domain == 'git.freedesktop.org' or domain == 'anongit.freedesktop.org' then
		return "http://cgit.freedesktop.org" .. no_git_path .. "/tree/" .. file .. branch_frag
	end
	if domain == "github.com" or domain:match('gitlab.') then
		if branch == nil then branch = "master" end
		no_git_path = no_git_path:gsub("%.git$", '')
		if file == "" then
			return "https://" .. domain .. no_git_path .. "/tree/" .. branch
		else
			return "https://" .. domain .. no_git_path .. "/blob/" .. branch .. "/" .. file
		end
	end

	-- Fallback
	return "http://" .. domain .. no_git_path .. "/tree/" .. file .. branch_frag
end
