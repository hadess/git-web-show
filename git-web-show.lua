#!/bin/lua

-- TODO
-- - Add support for gist.github.com
-- - Fix handling of directories

require 'lib.gws-lib'

-- Options
local print_url = false
local file = nil
for k, v in ipairs(arg) do
	if string.sub (v, 1, 2) == "--" then
		if v == '--print' then
			print_url = true
		else
			print ("Unknown option '" .. v .. "'")
			usage()
			return 1
		end
	else
		file = v
		break
	end
end

-- Remote URL
remote_url = get_remote_url()
if remote_url == '' or remote_url == nil then
	print ("There was a problem getting the remote URL for this git repository")
	return 1
end

-- Branch
local branch = get_stdout("git rev-parse --abbrev-ref HEAD")
branch = branch:gsub("\n", '')
if branch == "master" then branch = nil end

-- File if any
file = get_file(file)

-- The meat
local domain, path = get_domain(remote_url)
if not domain or not path then
	print ("There was a problem handling '".. remote_url .."'")
	return 1
end

local web_url = get_url(domain, path, branch, file)

-- Print or launch
if print_url == false then
	local cmd = "xdg-open " .. web_url
	os.execute (cmd)
else
	print (web_url)
end
